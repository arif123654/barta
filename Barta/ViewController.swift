//
//  ViewController.swift
//  Barta
//
//  Created by apple on 2/4/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import CarbonKit

class ViewController: UIViewController , CarbonTabSwipeNavigationDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let items = ["Top News", "Sports", "Travel", "Polotics","Entertainment", "Technology"]
        let carbonTapSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        //Start
        carbonTapSwipeNavigation.insert(intoRootViewController: self)
        carbonTapSwipeNavigation.toolbar.isTranslucent = false
        carbonTapSwipeNavigation.toolbar.barTintColor = UIColor.black
        
        carbonTapSwipeNavigation.setIndicatorColor(UIColor.orange)
        carbonTapSwipeNavigation.setTabBarHeight(20)
        carbonTapSwipeNavigation.setTabExtraWidth(16)
        
        //End
        carbonTapSwipeNavigation.toolbar.clipsToBounds = true
        carbonTapSwipeNavigation.setSelectedColor(UIColor.white, font: UIFont.systemFont(ofSize: 14))
        carbonTapSwipeNavigation.setNormalColor(UIColor.lightGray, font: UIFont.systemFont(ofSize: 14))
        carbonTapSwipeNavigation.carbonTabSwipeScrollView.bounces = false
        
       //set navigation bar with out title
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        
    }

    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        // return viewController at index
        let vc = storyboard?.instantiateViewController(identifier: "SearchViewController") as! SearchViewController
        
        return vc
    }
    
    

}

